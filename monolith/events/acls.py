from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import json
import requests


def get_photo(city, state):
    # Create a dictionary for the headers to use in the request
    headers = {"Authorization": PEXELS_API_KEY}
    abbreviation = {
        "query": city + " " + state,
        "per_page": 1,
    }
    # Create the URL for the request with the city and state
    url = "https://api.pexels.com/v1/search"
    # make the request
    response = requests.get(url, params=abbreviation, headers=headers)
    # Parse the JSON response
    content = json.loads(response.content)
    # Return a dictionary that contains a `picture_url` key and
    #   one of the URLs for one of the pictures in the response
    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except (KeyError, IndexError):
        return {"pictures_url": None}


def get_weather_data(city, state):
    params = {
        "q": f"{city}, {state}, US",
        "limit": 1,
        "appid": OPEN_WEATHER_API_KEY,
    }
    # Create the URL for the geocoding API with the city and state
    url = "http://api.openweathermap.org/geo/1.0/direct"
    # create a response object from the url
    response = requests.get(url, params=params)
    # deserialize the response into json
    content = json.loads(response.content)

    # first corruption layer
    try:
        latitude = content[0]["lat"]
        longitude = content[0]["lon"]
    except (KeyError, IndexError):
        return None

    params = {
        "lat": latitude,
        "lon": longitude,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
    }
    # process the request
    url = "http://api.openweathermap.org/data/2.5/weather"
    # want to get a new response back based on new parameters
    response = requests.get(url, params=params)
    # gotten response, now need to deserialize response
    content = json.loads(response.content)

    # now we need a new layer
    try:
        # return the data you just grabbed
        return {
            # find this in the docs what the key, value pair is
            "description": content["weather"][0]["description"],
            "temp": content["main"]["temp"],
        }
    except (KeyError, IndexError):
        content = {
            "main": {
                "temp": 0,
            }
        }
        return None
