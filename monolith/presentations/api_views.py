from unicodedata import name
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
from common.json import ModelEncoder, DataEncoder, QuerySetEncoder
from .models import Presentation
from events.models import Conference
from events.api_views import ConferenceListEncoder
import json
import pika


class PresentationDetailEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "presenter_name",
        "company_name",
        "presenter_email",
        "title",
        "synopsis",
        "created",
        "conference",
    ]
    encoders = {
        "conference": ConferenceListEncoder(),
    }

    def get_extra_data(self, o):
        return {"status": o.status.name}


class PresentationListEncoder(ModelEncoder):
    model = Presentation
    properties = ["title"]


@require_http_methods(["GET", "POST"])
def api_list_presentations(request, conference_id):
    if request.method == "GET":
        presentations = Presentation.objects.all()
        return JsonResponse(
            {"presentations": presentations}, encoder=PresentationListEncoder
        )
    # if anything other than a GET gets requested (in this case, only
    # other option is POST)
    else:
        content = json.loads(request.body)
        # check the conference to see if there is a conference ID that
        # matches the POST request
        try:
            conference = Conference.objects.get(id=conference_id)
            # make the conference variable accessible for instances of
            # the conference attached to that id
            content["conference"] = conference
        except Presentation.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid presentation"},
                status=400,
            )
        # once it's all confirmed, you can make a presentation using
        # he class creation method built in the model
        presentation = Presentation.create(**content)
        return JsonResponse(
            presentation,
            encoder=PresentationDetailEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_presentation(request, pk):
    if request.method == "GET":
        presentation = Presentation.objects.get(id=pk)
        return JsonResponse(
            presentation,
            encoder=PresentationDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Presentation.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        print("!!!!!", content)
        try:
            if "conference" in content:
                conference = Conference.objects.get(id=content["conference"])
                content["conference"] = conference
        except Presentation.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid presentation"}, status=400
            )
        Presentation.objects.filter(id=pk).update(**content)
        presentation = Presentation.objects.get(id=pk)
        return JsonResponse(
            presentation,
            encoder=PresentationDetailEncoder,
            safe=False,
        )


def presentation_message(message, queue_name):
    parameters = pika.ConnectionParameters(host="rabbitmq")
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    # create a queue if it doesn't exist
    channel.queue_declare(queue=queue_name)
    # send a message to the queue
    # exchange = message in rabbitmq can never be sent directly to the queue,
    # always needs to go through an exchange. "" specifies a default exchange
    # that allows us to specify exactly to which queue the message should go
    # routing_key = queue name needs to be specified
    # body = data from the producer
    channel.basic_publish(
        exchange="",
        routing_key=queue_name,
        body=message,
    )
    # close the connection to rabbitmq
    connection.close()


@require_http_methods(["PUT"])
def api_approve_presentation(request, pk):
    presentation = Presentation.objects.get(id=pk)
    presentation.approve()
    message = {
        "presenter_name": presentation.presenter_name,
        "presenter_email": presentation.presenter_email,
        "title": presentation.title,
    }
    presentation_message(json.dumps(message), "presentation_approvals")
    return JsonResponse(
        presentation,
        encoder=PresentationDetailEncoder,
        safe=False,
    )


@require_http_methods(["PUT"])
def api_reject_presentation(request, pk):
    presentation = Presentation.objects.get(id=pk)
    presentation.reject()
    message = {
        "presenter_name": presentation.presenter_name,
        "presenter_email": presentation.presenter_email,
        "title": presentation.title,
    }
    presentation_message(json.dumps(message), "presentation_rejections")
    return JsonResponse(
        presentation,
        encoder=PresentationDetailEncoder,
        safe=False,
    )
