import json
import pika
import django
import os
import sys
from django.core.mail import send_mail
from pika.exceptions import AMQPConnectionError

# tries to make a connection
import time


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()


# keep looping until it makes a connection (loop forever)
while True:
    try:
        # set the hostname that we'll connect to later
        parameters = pika.ConnectionParameters(host="rabbitmq")
        # create a connection to the hostn
        connection = pika.BlockingConnection(parameters)
        # open a channel to the host
        channel = connection.channel()
        # create a queue if it does not exist
        channel.queue_declare(queue="presentation_approvals")
        channel.queue_declare(queue="presentation_rejections")

        # create a function that will process the message when it arrives
        def process_approvals(ch, method, properties, body):
            message = json.loads(body)
            print("!!!!!!!!!!!!!", message)
            email = message["presenter_email"]
            name = message["presenter_name"]
            title = message["title"]
            send_mail(
                "Your presentation has been accepted",
                f"{name}, we're happy to tell you that your presentation {title} has been accepted",
                "admin@conference.go",
                [email],
                fail_silently=False,
            )

        def process_rejections(ch, method, properties, body):
            message = json.loads(body)
            print("!!!!!!!!!!!!!", message)
            email = message["presenter_email"]
            name = message["presenter_name"]
            title = message["title"]
            send_mail(
                "Your presentation has been rejected",
                f"{name}, we're sorry to tell you that your presentation {title} has been rejected",
                "admin@conference.go",
                [email],
                fail_silently=False,
            )

        # begin consuming messages from a queue, configures the consumer to call
        # the process_approvals function when a message arrives
        channel.basic_consume(
            queue="presentation_approvals",
            on_message_callback=process_approvals,
            # return any pending messages that arrived before broker confirmed the
            # cancellation
            auto_ack=True,
        )
        channel.basic_consume(
            queue="presentation_rejections",
            on_message_callback=process_rejections,
            auto_ack=True,
        )
        # tells rabbitmq that you're ready to receive messages
        channel.start_consuming()

    # if connection fails, print out a message and go to sleep for 2 sec, then try
    # to do it again
    except AMQPConnectionError:
        print("Unable to connect")
        time.sleep(2.0)
